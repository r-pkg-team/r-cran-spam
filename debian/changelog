r-cran-spam (2.11-1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Charles Plessy <plessy@debian.org>  Tue, 28 Jan 2025 08:48:16 +0900

r-cran-spam (2.11-0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)

 -- Charles Plessy <plessy@debian.org>  Thu, 10 Oct 2024 12:48:32 +0900

r-cran-spam (2.10-0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Sun, 29 Oct 2023 16:24:45 +0100

r-cran-spam (2.9-1-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 11 Aug 2022 16:04:52 +0200

r-cran-spam (2.9-0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 15 Jul 2022 11:38:51 +0200

r-cran-spam (2.8-0-1) unstable; urgency=medium

  * New upstream version
  * Drop patch applied upstream

 -- Andreas Tille <tille@debian.org>  Sat, 15 Jan 2022 10:44:11 +0100

r-cran-spam (2.7-0-2) unstable; urgency=medium

  * Team upload

  [ Andreas Tille ]
  * Disable reprotest

  [ Sébastien Villemot ]
  * buffer-overflow.patch: new patch, fixes autopkgtest on i386 in the context
    of the testing migration of lapack 3.10.0-1

 -- Sébastien Villemot <sebastien@debian.org>  Fri, 01 Oct 2021 15:30:51 +0200

r-cran-spam (2.7-0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 20 Aug 2021 07:35:10 +0200

r-cran-spam (2.6-0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Testsuite: autopkgtest-pkg-r (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Repository.

 -- Andreas Tille <tille@debian.org>  Sat, 16 Jan 2021 17:51:35 +0100

r-cran-spam (2.5-1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Dylan Aïssi <daissi@debian.org>  Fri, 03 Jan 2020 15:00:55 +0100

r-cran-spam (2.5-0-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Mon, 09 Dec 2019 11:38:16 +0100

r-cran-spam (2.4-0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.4.1

 -- Andreas Tille <tille@debian.org>  Thu, 14 Nov 2019 13:07:57 +0100

r-cran-spam (2.3-0-1) unstable; urgency=medium

  * New upstream version
  * debhelper-compat 12
  * Standards-Version: 4.4.0
  * Set upstream metadata fields: Archive, Bug-Database.

 -- Andreas Tille <tille@debian.org>  Mon, 16 Sep 2019 10:24:55 +0200

r-cran-spam (2.2-2-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Tue, 09 Jul 2019 22:15:39 +0200

r-cran-spam (2.2-1-1) unstable; urgency=medium

  * New upstream version
  * debhelper 12
  * Standards-Version: 4.3.0

 -- Andreas Tille <tille@debian.org>  Fri, 11 Jan 2019 10:54:30 +0100

r-cran-spam (2.2-0-1) unstable; urgency=medium

  * New upstream version
  * Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-
    lists.debian.net>
  * dh-update-R to update Build-Depends

 -- Andreas Tille <tille@debian.org>  Fri, 22 Jun 2018 21:17:00 +0200

r-cran-spam (2.1-4-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Sun, 15 Apr 2018 19:51:40 +0200

r-cran-spam (2.1-3-1) unstable; urgency=medium

  * New upstream version
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.1.4

 -- Andreas Tille <tille@debian.org>  Mon, 09 Apr 2018 22:39:25 +0200

r-cran-spam (2.1-2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 2.1-2
  * d/copyright: reflect upstream changes.
  * Bump to debhelper compat level 11.
  * Bump Standards-Version to 4.1.3.
  * Reactivate demo_jss15-Leroux.R test, now that truncdist is in Debian.

 -- Sébastien Villemot <sebastien@debian.org>  Tue, 20 Feb 2018 16:29:03 +0100

r-cran-spam (2.1-1-2) unstable; urgency=medium

  * Team upload.
  * Move packaging from SVN to Git.
  * Fix autopkgtest:
    + add missing r-cran-{testthat,fields} test dependencies
    + skip demo_jss15-Leroux.R test, needs truncdist which is not in Debian
    + update d/tests/run-unit-test so that it displays test results
  * Bump Standards-Version to 4.1.1.
  * d/watch: bump to file format v4, use secure URL.

 -- Sébastien Villemot <sebastien@debian.org>  Sun, 19 Nov 2017 15:46:50 +0100

r-cran-spam (2.1-1-1) unstable; urgency=medium

  * New upstream version
  * debhelper 10
  * use dh-r instead of cdbs
  * Standards-Version: 4.1.0 (no changes needed)
  * Canonical homepage for CRAN packages

 -- Andreas Tille <tille@debian.org>  Thu, 07 Sep 2017 22:16:13 +0200

r-cran-spam (1.0-1-1) unstable; urgency=medium

  * Initial release (Closes: #764731).

 -- Andreas Tille <tille@debian.org>  Fri, 10 Oct 2014 17:04:23 +0200
